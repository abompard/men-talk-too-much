# We talk too much

That's not even a question anymore. Men talk too much. They talk longer, and
more frequently than anybody else. Now prove it with stats!

The result page is available here: https://www.mentalktoomuch.info

It is a simple single-page-application with no backend. Everything happens in your browser, and the
app works offline (no need for an internet connection). If you're on a mobile device, you can add it
to your home screen and it'll look like a regular application.

Based on [ReactJS](https://reactjs.org), [Redux](https://redux.js.org) and [Material
UI](https://material-ui.com). Follows the [Progressive web
app](https://en.wikipedia.org/wiki/Progressive_web_application) standards.

The code is licensed under the [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)

Logo by [PomPrint](https://openclipart.org/detail/167032/trilby-hat) from
OpenClipArt.
