// translationRunner.js
const manageTranslations = require('react-intl-translations-manager').default;
 
// es2015 import
// import manageTranslations from 'react-intl-translations-manager';
 
manageTranslations({
  messagesDirectory: 'tmp/messages/',
  translationsDirectory: 'src/locales/',
  languages: ['fr'] // any language you need
});
