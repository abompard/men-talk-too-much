import { connect } from "react-redux";
import { startTalking, stopTalking } from "../actions";
import SpeakerButton from "../components/SpeakerButton";

const mapStateToProps = (state, ownProps) => {
  return {
    selected: ownProps.category === state.current.category
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    startTalking: () => dispatch(startTalking(ownProps.category)),
    stopTalking: () => dispatch(stopTalking())
  };
};

const ConnectedSpeakerButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(SpeakerButton);

export default ConnectedSpeakerButton;
