import React from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import grey from "@material-ui/core/colors/grey";
import { defineMessages, FormattedMessage } from "react-intl";

const styles = {
  root: {
    color: grey[600],
    marginRight: 0
  },
  icon: {
    color: grey[600]
  }
};

const messages = defineMessages({
  text: {
    id: "alreadytalkedbutton.text",
    defaultMessage: "Has already spoken"
  }
});

function AlreadyTalkedButton(props) {
  const { classes, disabled, hasAlreadyTalked, setAlreadyTalked } = props;

  const handleChange = e => {
    const value = e.target.checked;
    setAlreadyTalked(value);
  };

  return (
    <FormControlLabel
      disabled={disabled}
      className={classes.root}
      control={
        <Checkbox
          checked={hasAlreadyTalked}
          onChange={handleChange}
          size="small"
          value="alreadytalked"
          icon={
            <CheckBoxOutlineBlankIcon
              className={disabled ? null : classes.icon}
            />
          }
          checkedIcon={
            <CheckBoxIcon className={disabled ? null : classes.icon} />
          }
        />
      }
      label={<FormattedMessage {...messages.text} />}
    />
  );
}

export default withStyles(styles)(AlreadyTalkedButton);
